import logo from './logo.svg';
import './App.css';
import Tori from './Tori';
function App() {
  return (
    <div className="App">
      <Tori/>
    </div>
  );
}

export default App;
