import React from "react";
import "./Tori.css";
import {Card, Row, Col} from "react-bootstrap";

const Tori = () => {
  return (
    <Card className="tori">
      <Row className="header-content">
      <input type="text" placeholder="Hakusana ja/tai postinumero" className="search-text"/>
      <select className="what">
        <option id="1">Kaikki osastot</option>
      </select>
      <select className="where">
        <option id="1">Koko Suomi</option>
      </select>
      </Row>
      <Row>
        <Col>
          <div className="checkboxes-and-button">
            <input type="checkbox" className="check" />
            Myydään
            <input type="checkbox" className="check" />
            Ostetaan
            <input type="checkbox" className="check" />
            Vuokrataan
            <input type="checkbox" className="check" />
            Halutaan vuokrata
            <input type="checkbox" className="check" />
            Annetaan
            <button className="save-search">Tallenna haku</button>
            <button className="search-button">Hae</button>
          </div>
        </Col>
      </Row>
    </Card>
  );
};

export default Tori;
